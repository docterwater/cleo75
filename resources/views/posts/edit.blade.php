<h1>編輯文章</h1>

<form action="{{ route('posts.update', [ 'post' => $post]) }}" method="POST">
    @method('PUT')
    @csrf
    <label>內容：
        <textarea name="content">{{ $post->content }}</textarea>
    </label><br>
    <input type="submit" value="送出文章">
</form>

<form action="{{ route('posts.destroy', ['post' =>$post]) }}" method="post">
    @csrf
    @method('DELETE')
    <input type="submit" value="刪除文章">
</form>