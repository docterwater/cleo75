<?php

use App\Post;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\App;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/hello-world', function () {
    return view('hello_world');
});
Route::get('/inspire', 'InspiringController@inspire');



Route::get('/test/insert', function(){
//   \App\Post::create(array('content'=>'content4'));
//    return \App\Post::all();
    $post = new App\Post;
    $post->content = 'Laravel demo insert2';
    $post->save();
    return $post;
});

Route::get('/test', function(){
//    $tag = App\Tag::find(2);
//    $posts = $tag->posts;
//    return $posts;

    return Post::cursor()->filter(function ($post) {
        return $post->id < 50;
    });
});
Route::get('/test2', function(){
    $post = App\Post::find(5);
    $tags = $post->tags;
    return $tags;
});

Route::get('/test/update', function(){
    $post = \App\Post::all();
    $post->each->update(['content'=>'laravel all be MASS update']);
    return $post;
});
Route::get('/test/delete', function(){
    $post = Post::find(9);
    $post->delete();
});

Route::get('/test/massdelete', function(){
    $posts = App\Post::destroy([2, 4]);
    return $posts;
});

Route::get('/test_login', function(){
//    var_dump(Auth::check());
    echo Auth::user();
});

Route::resource('posts','PostController');


Route::get('/log', function(){

    Illuminate\Support\Facades\Log::info('觸發 Test');
});

Route::get('/crypt', function(){
    return decrypt(encrypt('123456789'));
});

Route::get('/trans', function(){
//    切換成原本的中文語系
//    return trans('auth.failed');

//    切換成原本的英文語系
    App::setLocale('en');
    return trans('auth.failed');
});
Route::get('/trans2', function(){
    return trans('auth.throttle', ['seconds' => 5]);
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
