<?php

namespace Tests\Feature;

use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     *
     */
    public function testStore()
    {
        $this->post(route('posts.store'), [
            'content' => 'Laravel 6.0 tutorial day 21'
        ]);

        $this->assertDatabaseHas('posts', [
            'content' => 'Laravel 6.0 tutorial day 21',
        ]);
    }
    public function testUpdate()
    {
        $post = new Post;
        $post->content = '';
        $post->subject_id = 0;
        $post->save();

        $this->put(route('posts.update', ['post' => $post]), [
            'content' => 'Laravel 6.0 tutorial day 21-2'
        ]);

        $this->assertDatabaseHas('posts', [
            'content' => 'Laravel 6.0 tutorial day 21-2',
        ]);
    }
    public function testDestroy()
    {
        $post = new Post;
        $post->content = 'Laravel 6.0 tutorial day 21-3';
        $post->subject_id = 0;
        $post->save();

        $this->delete(route('posts.destroy', ['post' => $post]));

//        $this->assertDatabaseMissing('posts', [
//            'content' => 'Laravel 6.0 tutorial day 21-3',
//        ]);
        $this->assertSoftDeleted('posts', [
            'content' => 'Laravel 6.0 tutorial day 21-4',
        ]);
    }
}
