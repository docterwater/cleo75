<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    //
    use SoftDeletes;
    protected $fillable =[
        'content',
        'subject_id'
    ];

    public function getContentAttribute($content)
    {
        return decrypt($content);
    }

    public function setContentAttribute($content)
    {
        $this->attributes['content'] = encrypt($content);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function tags(){
        return $this->belongsToMany(Tag::class);
    }

    public function user(){
        $this->belongsTo(User::class);
    }
}
