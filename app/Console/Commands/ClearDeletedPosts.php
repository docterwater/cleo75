<?php

namespace App\Console\Commands;

use App\Post;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ClearDeletedPosts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */

    //指令的呼叫方式，之後使用 php artisan posts:clear 就可以實作裡面的邏輯。

    protected $signature = 'posts:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'clear softdelet posts ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        Post::destroy([33,34]);
        $posts = Post::onlyTrashed()->get();
        foreach ($posts as $post){
            $post->forceDelete();
        }
        Log::info('移除所有被刪除文章');
        return;
    }
}
